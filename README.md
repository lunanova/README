# Information About My Codeberg

I'll be blunt. I don't like Gitea and its derivatives. I never have and I probably never will.

## Why don't you like Codeberg/Gitea?

Honestly, I have no idea. It's one of those things that really doesn't make sense and has no justification. I used to think it was because it wasn't as powerful as GitHub and GitLab, but I know that's not true. Then I thought it was because of the way it looked, but that's plain silly, so I don't know.

## Why do you have an account here?

Mainly so I can interact with projects on here, like [Calckey](https://codeberg.org/thatonecalculator/calckey) (which I [maintain a fork of](https://github.com/mint-lgbt/fedimint)), and so I have an "official" account so people don't take my name.

## Where are you actually posting your code?

Right now, [GitHub](https://github.com/lunaisnotaboy), but I'm trying to move it to a GitLab (be it the [shared one](https://gitlab.com/lunaisnotaboy) or a [self-hosted one](https://koakuma.soopy.moe/luna)). It'll take awhile, as I have over 25 organizations with around 10 repositories each, so the migration will not be immediate.

## Where can I contact you?

You can find me on [the Fediverse](https://fedi.mint.lgbt/@luna), as well as on [Matrix](https://matrix.to/#/@luna:the-apothecary.club). If you'd like to email me, [you can do that as well](mailto:her@mint.lgbt).
